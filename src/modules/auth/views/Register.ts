import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import { AxiosError, AxiosResponse } from 'axios';
import EmailUnique from '@/libs/mixins/EmailUnique';
import ConfirmPassword from '@/libs/mixins/ConfirmPassword';
@Component({
    template: require('./Register.html'),
})
export default class Register extends Mixins(EmailUnique, ConfirmPassword) {

    public $axios!: any;
    public form: any;
    public formData: object = {
        firstname: ['firstname'],
        lastname: ['lastname'],
        email: ['email', { rules: [{ validator: this.validateEmail }] }],
        password: ['password', {
            rules: [
                { required: true, message: 'Please input your password' },
                { validator: this.validateToNextPassword },
            ],
        }],
        password_confirm: ['password_confirm', {
            rules: [
                { required: true, message: 'Please confirm your password' },
                { validator: this.compareToFirstPassword },
            ],
        }],
    };
    public data() {
        return {
            form: this.$form.createForm(this),
        };
    }
    public submit(e: any) {
        e.preventDefault();
        this.form.validateFieldsAndScroll((err: any, form: any) => {
            if (!err) {
                this.$axios.post(`${process.env.VUE_APP_APIURL}/auth/register`, form)
                .then((response: AxiosResponse) => {
                    if (response.data.status === 'success') {
                        this.$message.success(response.data.message);
                        this.goToLogin();
                    } else {
                        this.$message.error(response.data.message);
                    }
                }).catch((error: AxiosError) => {
                    this.emailError();
                });
            }
        });
    }

    public goToLogin() {
        this.$router.push({
            name: 'login',
        });
    }
}
