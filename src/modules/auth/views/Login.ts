import {
  Component,
  Vue,
} from "vue-property-decorator";
import { Checkbox, Alert } from "ant-design-vue";
import { AxiosError, AxiosResponse } from 'axios';
import _get from 'lodash/get';

Vue.use(Checkbox);
Vue.use(Alert);

@Component({
  template: require('./Login.html'),
})
export default class Login extends Vue {

  public formData: object = {
    email: ['email', { rules: [{ required: true, type: 'email' }] }],
    password: ['password', { rules: [{ required: true, message: 'Please input your Password!' }] }],
    remember: ['remember', { valuePropName: 'checked', initialValue: true }],
  };
  public $axios!: any;
  public form: any;

  constructor() {
    super();
  }

  public data() {
    return {
      form: this.$form.createForm(this),
    };
  }

  public goToDesktop() {
    this.$router.push({
      name: 'deskop',
    });
  }

  public submit(e: any) {
    e.preventDefault();
    this.form.validateFieldsAndScroll((err: any, form: any) => {
      if (!err) {
        this.$axios.post(`${process.env.VUE_APP_APIURL}/auth/login`, form)
          .then((response: AxiosResponse) => {
            if (response.data.token) {
              this.$message.success("Sucessfully logged in!");
              localStorage.setItem('jwt', response.data.token);
              this.$axios.updateAuthToken();
              this.goToDesktop();
            } else {
              this.$message.error(_get(response, 'data.error'));
            }
          }).catch((error: AxiosError) => {
            if (_get(error, 'response.data.message', false)) {
              this.$message.error(_get(error, 'response.data.message'));
            }
          });
      }
    });
  }
}
