import {
  Component,
  Mixins,
} from 'vue-property-decorator';

import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import { Category } from '../../models/Category';
import _merge from 'lodash/merge';
import _getOr from "lodash/fp/getOr";
import _head from "lodash/fp/head";
import SlugUnique from '@/libs/mixins/SlugUnique';

@Component({
  template: require("./Create.html"),

})
export default class Create extends Mixins( FormMixin, SlugUnique) {

  public formData: object = {
    name: ['name', { rules: [{ required: true }] }],
    slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
  };

  get currentNode(): any {
    return _head(this.$store.state.product.selectedNodes);
  }

  public submit(e: any) {
    e.preventDefault();
    this.form.validateFieldsAndScroll((err: any, form: Category) => {
      if (!err) {
        this.$axios.post(`${process.env.VUE_APP_APIURL}/product/categories/append`, {
          parent: this.currentNode,
          form,
        }).then((res: AxiosResponse) => {
          this.$store.dispatch('product/getCategoryList');
          this.back();
        }).catch((error: AxiosError) => {
          console.log(error);
        });
      }
    });
  }
  public back() {
    this.$router.push({
      name: "product.categories.index",
    });
  }
  public setBreadcrumbs() {
    this.$store.commit('setBreadcrumbs', [
      {
        name: "Deskop",
        route: "deskop",
      },
      {
        name: "Product categories",
        route: "product.categories.index",
      },
      {
        name: "Create category",
        route: null,
      },
    ]);
  }

  public async mounted() {
    this.setBreadcrumbs();
  }
}
