import {
    Component,
    Mixins,
    Watch,
} from 'vue-property-decorator';

import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import { Category } from '../../models/Category';
import _merge from 'lodash/merge';
import SlugUnique from '@/libs/mixins/SlugUnique';

@Component({
    template: require("./Edit.html"),
})
export default class Edit extends Mixins( FormMixin, SlugUnique) {

    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
    };

    public getFormData() {
        this.$axios.get(`${process.env.VUE_APP_APIURL}/product/categories/edit`, {
            params: {
                id: this.$route.params.id,
            },
        }).then((res: any) => {
            this.setDataFields(this.form.getFieldsValue(), res.data.model);
        });
    }
    public back() {
        this.$router.push({
            name: "product.categories.index",
        });
    }
    public submit() {
        this.form.validateFieldsAndScroll((err: any, form: Category) => {
            if (!err) {
                form = _merge(form, { _id: this.$route.params.id });
                this.$axios.post(`${process.env.VUE_APP_APIURL}/product/categories/update`, form)
                .then((response: AxiosResponse) => {
                    this.$message.success('Category was successfully updated');
                    this.$store.dispatch('product/getCategoryList');
                    this.back();
                }).catch((error: AxiosError) => {
                    console.log(error, 'error');
                    this.slugError();
                });
            }
        });
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [
            {
                name: "Deskop",
                route: "deskop",
            },
            {
                name: "Product categories",
                route: "product.categories.index",
            },
            {
                name: "Edit category",
                route: null,
            },
        ]);
    }
    @Watch('$route')
    public onchange(to, from) {
        this.getFormData();
    }
    public async mounted() {
        this.getFormData();
        this.setBreadcrumbs();
    }
}
