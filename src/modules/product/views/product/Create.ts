import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import { Product } from '../../models/Product';
import _merge from 'lodash/merge';
import { InputNumber, Tree } from 'ant-design-vue';
import SlugUnique from '@/libs/mixins/SlugUnique';
import FileChooser from '@/modules/media/components/FileChooser';

@Component({
    template: require("./Create.html"),
    components: {
        'a-editor': Editor,
        'a-input-number': InputNumber,
        'a-tree': Tree,
        FileChooser,
    },
})
export default class Create extends Mixins(FormMixin, SlugUnique) {

    public categoriesLoaded: boolean = false;
    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{validator: this.validateSlug}] }],
        description: ['description', { rules: [{ required: true }] }],
        price: ['price', { rules: [{ required: true, type: 'number' }] }],
        categories: ['categories'],
        status: ['status'],
        thumbnail: ['singleFiles.thumbnail'],
        gallery: ['multipleFiles.gallery'],
    };

    get categories() {
        return this.$store.state.product.categoryList;
    }

    public categoryCheck(value: any) {
        this.form.setFieldsValue({
            categories: value.checked,
        });
    }
    public back() {
        this.$router.push({
            name: "product.products.index",
        });
    }
    public submit() {
        this.form.validateFieldsAndScroll((err: any, form: Product) => {
            if (!err) {
                this.$axios.post(`${process.env.VUE_APP_APIURL}/product/products/store`, form)
                .then((response: AxiosResponse) => {
                    this.$message.success('Product was successfully added');
                    this.back();
                })
                .catch((error: AxiosError) => {
                    this.slugError();
                });
            }
        });
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [
            {
                name: "Deskop",
                route: "deskop",
            },
            {
                name: "Products",
                route: "product.products.index",
            },
            {
                name: "Create product",
                route: null,
            },
        ]);
    }

    public mounted() {
        this.$store.dispatch('product/getCategoryList');
        this.setBreadcrumbs();
    }
}
