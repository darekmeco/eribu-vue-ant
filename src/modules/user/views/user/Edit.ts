import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import { User } from '../../models/User';
import { AxiosResponse, AxiosError } from 'axios';
import FormMixin from '@/libs/mixins/FormMixin';
import { Tabs } from 'ant-design-vue';
import _merge from 'lodash/merge';
import EmailUnique from '@/libs/mixins/EmailUnique';
import Permission from '../../components/Permission.vue';
import ResetPassword from '@/libs/mixins/ResetPassword';
import FileChooser from '@/modules/media/components/FileChooser';

@Component({
    template: require("./Edit.html"),
    components : {
        'a-tabs': Tabs,
        'a-tab-pane': Tabs.TabPane,
        'permission': Permission,
        FileChooser,
    },
})
export default class Edit extends Mixins(FormMixin, EmailUnique, ResetPassword) {

    public formData: object = {
        firstname: ['firstname'],
        lastname: ['lastname'],
        email: ['email', { rules: [{ validator: this.validateEmail }] }],
        role: ['role', { rules: [{ required: true, message: 'Role is required' }] }],
        permissions: ['permissions'],
        thumbnail: ['singleFiles.thumbnail'],
    };
    public resetPassData: object = {
        current_password: ['current_password', {
            rules: [
                { required: true, message: 'Please input your current password' },
            ],
        }],
        password: ['password', {
            rules: [
                { required: true, message: 'Please input new password' },
                { validator: this.validateToNextPassword },
            ],
        }],
        password_confirm: ['password_confirm', {
            rules: [
                { required: true, message: 'Please confirm your new password' },
                { validator: this.compareToFirstPassword },
            ],
        }],
    };

    get resetPassForm() {
        return this.$form.createForm(this);
    }
    get roles() {
        return this.$store.state.user.allRoles;
    }
    get currentUser() {
        return this.$store.state.currentUser;
    }

    public getFormData() {
        this.$axios.get(`${process.env.VUE_APP_APIURL}/user/users/edit`, {
            params: {
                id: this.$route.params.id,
            },
        }).then((res: any) => {
            this.setDataFields(this.form.getFieldsValue(), res.data.model);
        });
    }
    public submit() {
        this.form.validateFieldsAndScroll(
            (err: any, form: User) => {
                if (!err) {
                    form = _merge(form, { _id: this.$route.params.id });
                    console.log(form, 'form');
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/user/users/update`, form)
                        .then((response: AxiosResponse) => {
                            if (this.$route.params.id === this.currentUser._id) {
                                this.$store.dispatch('getCurrentUser');
                            }
                            this.$message.success('User was successfully updated');
                            this.back();
                        }).catch((error: AxiosError) => {
                            console.log(error, 'error');
                            this.emailError();
                        });
                }
            },
        );
    }
    public changePassword() {
        this.resetPassForm.validateFields(
            (err: any, resetPassForm: any) => {
                if (!err) {
                    console.info('changepass', resetPassForm);
                    resetPassForm = _merge(resetPassForm, { _id: this.$route.params.id });
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/user/users/change-password`, resetPassForm)
                        .then((response: AxiosResponse) => {
                            this.$message.success('Password successfully changed');
                        })
                        .catch((error: AxiosError) => {
                            console.log(error, 'error');
                        });
                }
            },
        );
    }

    public back() {
        this.$router.push({
            name: 'user.users.index',
        });
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Users',
            route: 'user.users.index',
        },
        {
            name: 'Edit user',
            route: null,
        },
        ]);
    }

    public async mounted() {
        this.$store.dispatch('allRoles');
        this.getFormData();
        this.setBreadcrumbs();
    }
}
