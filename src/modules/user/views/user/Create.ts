import {
    Component,
    Mixins,
  } from 'vue-property-decorator';

import { User } from '../../models/User';
import { AxiosResponse, AxiosError } from 'axios';
import FormMixin from '@/libs/mixins/FormMixin';
import EmailUnique from '@/libs/mixins/EmailUnique';
import ConfirmPassword from '@/libs/mixins/ConfirmPassword';
import { Tabs } from 'ant-design-vue';
import Permission from '../../components/Permission.vue';

@Component({
    template: require("./Create.html"),
    components: {
        'a-tabs': Tabs,
        'a-tab-pane': Tabs.TabPane,
        'permission': Permission,
    },
})
export default class Create extends Mixins( FormMixin, EmailUnique, ConfirmPassword) {

    public formData: object = {
        firstname: ['firstname'],
        lastname: ['lastname'],
        email: ['email', { rules: [{ validator: this.validateEmail }] }],
        role: ['role', {rules: [{ required: true, message: 'Role is required' }]}],
        password: ['password', {
            rules: [
                { required: true, message: 'Please input your password' },
                { validator: this.validateToNextPassword },
            ],
        }],
        password_confirm: ['password_confirm', {
            rules: [
                { required: true, message: 'Please confirm your password' },
                { validator: this.compareToFirstPassword },
            ],
        }],
        thumbnail: ['singleFiles.thumbnail'],
        permissions: ['permissions'],
    };

    get roles() {
        return this.$store.state.user.allRoles;
    }

    public submit() {
        this.form.validateFieldsAndScroll(
            (err: object, form: User ) => {
                if (!err) {
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/user/users/store`, form)
                    .then((response: AxiosResponse) => {
                        this.$message.success('User was successfully added');
                        this.back();
                    }).catch((error: AxiosError) => {
                        console.log(error, 'error');
                        this.emailError();
                    });
                }
            },
        );
    }
    public back() {
        this.$router.push({
            name: 'user.users.index',
        });
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Users',
            route: 'user.users.index',
        },
        {
            name: 'Create user',
            route: 'user.users.create',
        },
        ]);
    }

    public mounted() {
        this.$store.dispatch('allRoles');
        this.setBreadcrumbs();
    }
}
