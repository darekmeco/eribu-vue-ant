import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import TableMixin from '@/libs/mixins/TableMixin';

@Component({
    template: require("./Index.html"),
})
export default class Index extends Mixins(TableMixin) {

    public columns = [{
        title: 'Name',
        sorter: true,
        key: 'name',
        scopedSlots: {
            customRender: 'name',
        },
    }, {
        title: 'Slug',
        sorter: true,
        key: 'slug',
        scopedSlots: {
            customRender: 'slug',
        },
    }, {
        title: "Created at",
        sorter: true,
        key: 'createdAt',
        scopedSlots: {
            customRender: 'createdAt',
        },
    }, {
        title: "Actions",
        scopedSlots: {
            customRender: 'actions',
        },
    },
    ];

    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Roles',
            route: 'user.roles.index',
        },
        ]);
    }

    public mounted() {
        this.setBreadcrumbs();
        // this.$store.commit('table/setPagination', {pageSize: 20});
    }
}
