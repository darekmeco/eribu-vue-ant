import {
    Component,
    Mixins,
} from 'vue-property-decorator';

import { Role } from '../../models/Role';
import { AxiosResponse, AxiosError } from 'axios';
import FormMixin from '@/libs/mixins/FormMixin';
import SlugUnique from '@/libs/mixins/SlugUnique';
import { Tabs } from 'ant-design-vue';
import Permission from '../../components/Permission.vue';


@Component({
    template: require("./Create.html"),
    components: {
        'a-tabs': Tabs,
        'a-tab-pane': Tabs.TabPane,
        'permission': Permission,
    },
})
export default class Create extends Mixins( FormMixin, SlugUnique) {

    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
        permissions: ['permissions'],
    };

    public submit() {
        this.form.validateFieldsAndScroll(
            (err: object, form: Role) => {
                if (!err) {
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/user/roles/store`, form)
                    .then((response: AxiosResponse) => {
                        this.$message.success('Role was successfully added');
                        this.back();
                    }).catch((error: AxiosError) => {
                        console.log(error, 'error');
                        this.slugError();
                    });
                }
            },
        );
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Roles',
            route: `user.roles.index`,
        },
        {
            name: `Create role`,
            route: `user.roles.create`,
        },
        ]);
    }
    public back() {
        this.$router.push({
            name: `user.roles.index`,
        });
    }

    public mounted() {
        this.setBreadcrumbs();
    }
}
