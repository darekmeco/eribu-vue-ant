export default interface ModulePermissionInterface {
    label: string;
    name: string;
    index: number;
    create: number;
    edit: number;
    update: number;
    delete: number;
}
