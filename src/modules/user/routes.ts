import App from '@/App.vue';
import Layout from '@/views/Layout';


const routes = {
    path: '/user',
    component: Layout,
    meta: { secure: true },
    children: [{
        path: 'users/index',
        name: 'user.users.index',
        component: () => import( /* webpackChunkName: "about" */ './views/user/Index'),
    },
    {
        path: 'users/create',
        name: 'user.users.create',
        component: () => import( /* webpackChunkName: "about" */ './views/user/Create'),
    },
    {
        path: 'users/edit/:id',
        name: 'user.users.edit',
        component: () => import( /* webpackChunkName: "about" */ './views/user/Edit'),
    },
    {
        path: 'roles/index',
        name: 'user.roles.index',
        component: () => import( /* webpackChunkName: "about" */ './views/role/Index'),
    },
    {
        path: 'roles/create',
        name: 'user.roles.create',
        component: () => import( /* webpackChunkName: "about" */ './views/role/Create'),
    },
    {
        path: 'roles/edit/:id',
        name: 'user.roles.edit',
        component: () => import( /* webpackChunkName: "about" */ './views/role/Edit'),
    },
    ],
};

export default routes;
