import { Role } from './Role';

export interface User {
    _id: string;
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    password_confirm: string;
    singleFiles: object[];
    role: Role;
}
