import { Module } from 'vuex';
import axios from 'axios';
import { Role } from './models/Role';
interface UserState {
    allRoles: Role[];
}

const user: Module<UserState, any> = {
    state: {
        allRoles: [],
    },
    mutations: {
        setAllRoles(state, items: Role[]) {
            state.allRoles = items;
        },
    },
    actions: {
        allRoles(context: any, params: any) {
            axios.get(`${process.env.VUE_APP_APIURL}/user/roles/all`, {
                params,
            }).then((response) => {
                context.commit('setAllRoles', response.data);
            });
        },
    },
    getters: {
        allRoles(state) {
            return state.allRoles;
        },
    },
};

export default user;
