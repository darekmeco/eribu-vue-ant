import { Module } from 'vuex';
import { Category } from './models/Category';
import axios from 'axios';

interface BlogState {
    allCategories: Category[];
}

const blog: Module<BlogState, any> = {
    namespaced: true,
    state: {
        allCategories: [],
    },
    mutations: {
        setAllCategories(state, items: Category[]) {
            state.allCategories = items;
        },
    },
    actions: {
        allCategories(context: any, params: any) {
            axios.get(`${process.env.VUE_APP_APIURL}/blog/categories/all`, {
                params,
            }).then((response) => {
                context.commit('setAllCategories', response.data);
            });
        },
    },
    getters: {
        allCategories(state) {
            return state.allCategories;
        },
    },
};

export default blog;
