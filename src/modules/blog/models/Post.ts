import File from '@/modules/media/models/File';

export interface Post {
    _id: string;
    title: string;
    slug: string;
    content: string;
    singleFiles: File[];
}
