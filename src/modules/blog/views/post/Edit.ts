import {
    Component,
    Mixins,
} from 'vue-property-decorator';

import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import { Post } from '../../models/Post';
import _merge from 'lodash/merge';
import SlugUnique from '@/libs/mixins/SlugUnique';

@Component({
    template: require("./Edit.html"),
    components: {
        'a-editor': Editor,
    },
})
export default class Edit extends Mixins( FormMixin, SlugUnique) {

    public formData: object = {
        title: ['title', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
        content: ['content', { rules: [{ required: true }] }],
        category: ['category', { rules: [{ required: true, message: 'Category is required' }]}],
        thumbnail: ['singleFiles.thumbnail'],
    };

    get categories() {
        return this.$store.state.blog.allCategories;
    }

    public getFormData() {
        this.$axios.get(`${process.env.VUE_APP_APIURL}/blog/posts/edit`, {
            params: {
                id: this.$route.params.id,
            },
        }).then((res: any) => {
            this.setDataFields(this.form.getFieldsValue(), res.data.model);
        });
    }
    public submit() {
        this.form.validateFieldsAndScroll(
            (err: any, form: Post) => {
                if (!err) {
                    form = _merge(form, { _id: this.$route.params.id });
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/blog/posts/update`, form)
                    .then((response: AxiosResponse) => {
                        this.$message.success('Post was successfully updated');
                        this.back();
                    }).catch((error: AxiosError) => {
                        this.slugError();
                    });
                }
            },
        );
    }
    public back() {
        this.$router.push({
            name: 'blog.posts.index',
        });
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Posts',
            route: 'blog.posts.index',
        },
        {
            name: 'Edit post',
            route: null,
        },
        ]);
    }

    public mounted() {
        this.$store.dispatch('blog/allCategories');
        this.getFormData();
        this.setBreadcrumbs();
    }
}
