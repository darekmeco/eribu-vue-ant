import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import TableMixin from '@/libs/mixins/TableMixin';

@Component({
    template: require("./Index.html"),
})
export default class Index extends Mixins(TableMixin) {
    public columns = [{
        title: 'Name',
        key: 'name',
        sorter: true,
        scopedSlots: {
            customRender: 'name',
        },
    }, {
        title: 'Slug',
        key: 'slug',
        sorter: true,
        scopedSlots: {
            customRender: 'slug',
        },
    }, {
        title: "Created at",
        key: 'createdAt',
        sorter: true,
        scopedSlots: {
            customRender: 'createdAt',
        },
    }, {
        title: "Actions",
        scopedSlots: {
            customRender: 'actions',
        },
    },
    ];

    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Categories',
            route: 'blog.categories.index',
        },
        ]);
    }
    public mounted() {
        this.setBreadcrumbs();
    }
}

