import {
    Component,
    Mixins,
} from 'vue-property-decorator';

import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import { Category } from '../../models/Category';
import SlugUnique from '@/libs/mixins/SlugUnique';

@Component({
    template: require("./Create.html"),
    components: {
        'a-editor': Editor,
    },
})
export default class Create extends Mixins(FormMixin, SlugUnique) {

    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
    };

    public submit() {
        this.form.validateFieldsAndScroll(
            (err: object, form: Category) => {
                if (!err) {
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/blog/categories/store`, form)
                        .then((response: AxiosResponse) => {
                            this.$message.success('Category was successfully added');
                            this.back();
                        }).catch((error: AxiosError) => {
                            this.slugError();
                        });
                }
            },
        );
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Categories',
            route: `blog.categories.index`,
        },
        {
            name: `Create category`,
            route: `blog.categories.create`,
        },
        ]);
    }
    public back() {
        this.$router.push({
            name: `blog.categories.index`,
        });
    }

    public mounted() {
        this.setBreadcrumbs();
    }
}





