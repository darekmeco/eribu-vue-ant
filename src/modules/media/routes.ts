import Layout from '@/views/Layout';
const routes = {
  meta: { secure: true },
  path: '/media',
  component: Layout,
  children: [{
    path: 'index',
    name: 'media',
    component: () => import( /* webpackChunkName: "about" */ '@/modules/media/views/Index'),
  },
  ],
};

export default routes;
