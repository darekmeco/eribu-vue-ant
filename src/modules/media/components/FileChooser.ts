import {
    Component,
    Vue,
    Prop,
    Watch,
} from "vue-property-decorator";
import MediaTable from '@/modules/media/components/MediaTable';
import MediaManager from '@/modules/media/components/MediaManager.vue';
import _union from 'lodash/union';
import _remove from 'lodash/remove';

@Component({
    template: require('./FileChooser.html'),
    components: {
        MediaTable,
        MediaManager,
    },

})
export default class FileChooser extends Vue {

    private isFileChooserActive: boolean = false;
    private $fs: any;
    private files: File[] = [];

    @Prop({ default: false })
    private multiple: boolean;

    @Prop()
    private value: File[];

    get hideModalAfterPickFile() {
        return !this.multiple;
    }

    private fileInserted(file: File) {
        this.files = _union(this.files, [file]);
        if (!this.multiple)
            this.isFileChooserActive = false;
    }

    private removeFile(index: number) {
        _remove(this.files, (item, i) => {
            return index === i;
        })
        this.$forceUpdate();
    }

    @Watch('files')
    onFilesChange(value: File[]) {
        this.$emit('change', value);
    }

    @Watch('value')
    onValueChange(value: File[]) {
        this.files = value;
    }
}
