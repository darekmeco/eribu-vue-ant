import {
  Component,
  Mixins,
} from 'vue-property-decorator';
import FormMixin from '@/libs/mixins/FormMixin';
import SlugUnique from '@/libs/mixins/SlugUnique';
import { Popover, notification } from 'ant-design-vue';

export interface AppendItem {
  currentNode: string;
}

@Component({
  template: require("./AppendItem.html"),
  props: {
    showForm: Boolean,
    currentNode: String,
  },
  components: {
    "a-popover": Popover,
  },
})
export class AppendItem extends Mixins(FormMixin, SlugUnique) {
  public formData: object = {
    name: ['name', { rules: [{ required: true }] }],
    slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
  };
  /**
   * Send Menu item Form to backend
   * @param  e [description]
   * @return   [description]
   */
  public handleItemSubmit(e) {
    e.preventDefault();
    this.form.validateFields((err, values) => {
      console.log(values);
      values.parentid = this.currentNode;
      this.$axios.post(`${process.env.VUE_APP_APIURL}/menus/item/append`, values)
        .then((res) => {
          console.log('move res:', res);
          this.$emit('item-appended');
        }).catch((error) => {
          const { response: { data: { error: message = "", message: description = "" } } } = error;
          notification.error({
            message,
            description,
          });
        });

      if (!err) {
        console.log('Received values of form: ', values);
      }

    });
  }
  private hasErrors(fieldsError) {
    return Object.keys(fieldsError).some((field) => fieldsError[field]);
  }
}
