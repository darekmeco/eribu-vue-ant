import { Component, Mixins, Emit, Prop, Watch } from 'vue-property-decorator';
import FormMixin from '@/libs/mixins/FormMixin';
import SlugUnique from '@/libs/mixins/SlugUnique';
@Component({
  template: require("./EditForm.html"),
})
export class EditForm extends Mixins(FormMixin, SlugUnique) {
  public formFields: object = {
    name: ['name', { rules: [{ required: true }] }],
    slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
  };
  @Prop(Object) public formData: object;
  @Emit()
  public back() { }
  @Emit()
  public submit() { }

  @Watch('formData')
  public onFormDataChanged(val, oldVal) {
    console.log('formdata changed', val);
    this.setDataFields(this.form.getFieldsValue(), this.formData);
  }
  public mounted() {
    // console.log(this.formData, 'formadata');
    // this.setDataFields(this.form.getFieldsValue(), this.formData);
  }
}
