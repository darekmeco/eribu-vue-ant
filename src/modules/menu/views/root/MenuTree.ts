import {
  Component,
  Mixins,
} from 'vue-property-decorator';
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import { merge as _merge, head as _head, isEmpty as _isEmpty, get as _get } from 'lodash';
import { Tree, Popover, Alert } from 'ant-design-vue';
import SlugUnique from '@/libs/mixins/SlugUnique';
import { MenuItem } from '../../models/MenuItem';
import Dialogs from '@/libs/mixins/Dialogs';
import { EditForm, AppendItem } from '@/modules/menu/components';
@Component({
  template: require("./MenuTree.html"),
  components: {
    "a-tree": Tree,
    "a-popover": Popover,
    "append-item": AppendItem,
    "edit-form": EditForm,
    "a-alert": Alert,
  },
})
export default class Edit extends Mixins(
  Dialogs,
  FormMixin,
  SlugUnique) {


  public defaultExpandAll: boolean = true;
  public nodeList: object[] = [];
  public responseData: object = {};
  private selectedNodesInfo: object[] = [];

  /**
   * Tree for current menu
   * @return any
   */
  get nodeTree(): any {
    return this.$store.state.menu.nodeTree;
  }
  /**
   * Currently selected node
   * @return any
   */
  get currentNode(): any {
    return _head(this.$store.state.menu.selectedNodes);
  }
  /**
   * Currently selected nodes
   * @return string[]
   */
  get selectedNodes(): string[] {
    return this.$store.state.menu.selectedNodes;
  }

  public onSelect(selectedKeys, info) {

    const node = _head(selectedKeys);
    if (node) {
      this.$store.commit('menu/setSelectedNodes', node);
    }
    this.$router.push({
      name: 'menu.child.edit',
      params: { childId: this.currentNode },
    });
  }
  public onDrop(info: any) {
    this.confirmCustomMove(() => {
      const data: any = {};
      data.dropKey = info.node.eventKey;
      data.dragKey = info.dragNode.eventKey;
      data.dropPos = info.node.pos.split("-");
      data.dropPosition = info.dropPosition - Number(data.dropPos[data.dropPos.length - 1]);
      this.$axios
        .post(`${process.env.VUE_APP_APIURL}/menus/item/move`, data)
        .then((res: AxiosResponse) => {
          this.$store.dispatch('menu/getNodeTree', this.$route.params.id);
        })
        .catch((error: any) => {
          console.log(error, 'error');
        });
    });
  }
  public itemAppended() {
    console.log('itemAppended handler');
    this.$store.dispatch('menu/getNodeTree', this.$route.params.id);
  }
  public deleteItem(context: any, withChildren: boolean) {
    this.confirmCustomDelete(() => {
      this.$axios.delete(`${process.env.VUE_APP_APIURL}/menus/item/delete`, {
        data: {
          id: this.currentNode,
          all: withChildren,
        },
      }).then((res: AxiosResponse) => {
        this.$store.dispatch('menu/getNodeTree', this.$route.params.id);
      }).catch((error: AxiosError) => {
        //
        // this.$message.error(_get(res, 'data.error'));
        console.log('Menu MenuItem Edit.ts AxiosError', error);
      });
    });
  }
  public mounted() {
    this.$store.dispatch('menu/getNodeTree', this.$route.params.id);
    this.setBreadcrumbs();
  }


  public back() {
    this.$router.push({
      name: "menu.root.index",
    });
  }
  public setBreadcrumbs() {
    this.$store.commit('setBreadcrumbs', [
      {
        name: "Deskop",
        route: "deskop",
      },
      {
        name: "Menu",
        route: "menu.root.index",
      },
      {
        name: "Edit menu",
        route: null,
      },
    ]);
  }

}
