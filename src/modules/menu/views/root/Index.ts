import {
  Component,
  Mixins,
} from 'vue-property-decorator';
import TableMixin from '@/libs/mixins/TableMixin';
import FormatPrice from '@/libs/mixins/FormatPrice';

@Component({
  template: require("./Index.html"),
})
export default class Index extends Mixins(TableMixin, FormatPrice) {

  public columns = [{
    title: "Name",
    scopedSlots: {
      customRender: "name",
    },
    onFilter: (value: string, record: any) => record.name.indexOf(value) === 0,
    sorter: (a: any, b: any) => a.name.length - b.name.length,
  },
  {
    title: "Slug",
    scopedSlots: {
      customRender: "slug",
    },
    onFilter: (value: string, record: any) => record.name.indexOf(value) === 0,
    sorter: (a: any, b: any) => a.slug.length - b.slug.length,
  },
  {
    title: "Created at",
    dataIndex: "createdAt",
    key: "createdAt",
    scopedSlots: {
      customRender: "createdAt",
    },
    sorter: (a: any, b: any) => a.createdAt - b.createdAt,
  },
  {
    title: "Actions",
    scopedSlots: {
      customRender: 'actions',
    },
  },
  ];

  public setBreadcrumbs() {
    this.$store.commit('setBreadcrumbs', [{
      name: "Deskop",
      route: "deskop",
    },
    {
      name: "Products",
      route: null,
    },
    ]);
  }

  public mounted() {
    this.setBreadcrumbs();
  }
}
