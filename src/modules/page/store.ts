import { Module } from 'vuex';
import _merge from 'lodash/merge';

// interface PageState {}

const page: Module<object, any> = {
    namespaced: true,
    state: {},
    mutations: {},
    actions: {},
    getters: {},
};

export default page;
