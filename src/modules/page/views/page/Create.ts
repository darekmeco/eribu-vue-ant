import {
    Component,
    Mixins,
  } from 'vue-property-decorator';
import Editor from '@/components/form/Editor.vue';
import { Collapse } from "ant-design-vue";
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import { Page } from '../../models/Page';
import SlugUnique from '@/libs/mixins/SlugUnique';
import FileChooser from '@/modules/media/components/FileChooser';

@Component({
    template: require("./Create.html"),
    components: {
        'a-editor': Editor,
        "a-collapse": Collapse,
        "a-collapse-panel": Collapse.Panel,
        'file-chooser': FileChooser,
    },
})
export default class Create extends Mixins(FormMixin, SlugUnique) {

    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
        content: ['content', { rules: [{ required: true }] }],
        meta_title: ['meta_title'],
        meta_description: ['meta_description'],
        og_title: ['og_title'],
        og_description: ['og_description'],
        og_type: ['og_type'],
        thumbnail: ['singleFiles.thumbnail'],
        gallery: ['multipleFiles.gallery'],
    };

    public submit() {
        this.form.validateFieldsAndScroll(
            (err: any, form: Page) => {
                if (!err) {
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/page/pages/store`, form)
                    .then((response: AxiosResponse) => {
                        this.$message.success('Page was successfully added');
                        this.back();
                    }).catch((error: AxiosError) => {
                        this.slugError();
                    });
                }
            },
        );
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Pages',
            route: 'page.pages.index',
        },
        {
            name: 'Create page',
            route: null,
        },
        ]);
    }
    public back() {
        this.$router.push({
            name: 'page.pages.index',
        });
    }

    public mounted() {
        this.setBreadcrumbs();
    }
}
