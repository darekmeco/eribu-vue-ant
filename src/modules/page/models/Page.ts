import File from '@/modules/media/models/File';
import { MetaTagsIntergace } from '@/libs/interfaces/MetaTagsInterface';

export interface Page extends MetaTagsIntergace {
    _id: string;
    name: string;
    body: string;
    singleFiles: File[];
}
