import File from '@/modules/media/models/File';
export interface Block {
    _id: string;
    name: string;
    body: string;
    singleFiles: File[];
}
