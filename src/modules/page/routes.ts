import Layout from '@/views/Layout';
import EmptyRouter from '@/views/EmptyRouter.vue';

const routes = {
    path: '/page',
    meta: { secure: true },
    component: Layout,
    children: [{
        path: 'pages',
        component: EmptyRouter,
        children: [{
            path: 'index',
            name: 'page.pages.index',
            component: () => import('./views/page/Index'),
        },
        {
            path: 'create',
            name: 'page.pages.create',
            component: () => import('./views/page/Create'),
        },
        {
            path: 'edit/:id',
            name: 'page.pages.edit',
            component: () => import('./views/page/Edit'),
        },
    ],
    },
    {
        path: 'blocks',
        component: EmptyRouter,
        children: [
            {
                path: 'index',
                name: 'page.blocks.index',
                component: () => import('./views/block/Index'),
            },
            {
                path: 'create',
                name: 'page.blocks.create',
                component: () => import('./views/block/Create'),
            },
            {
                path: 'edit/:id',
                name: 'page.blocks.edit',
                component: () => import('./views/block/Edit'),
            },
        ],
    },
    ],
};

export default routes;
