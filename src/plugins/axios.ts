import Vue from 'vue';
import Axios from '@/libs/axios';
const Plugin = {
  install(VueInstance: any) {
    const axiosInstance = new Axios();
    VueInstance.prototype.$axios = axiosInstance;
  },
};
Vue.use(Plugin);
export default Plugin;
