"use strict";
import Vue from 'vue';
import fs from '@/libs/fs.ts';
const Fs = {};
Fs.install = function(Vue) {
  Vue.prototype.$fs = fs;
 };
Vue.use(Fs);
//export default Fs;
