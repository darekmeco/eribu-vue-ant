"use strict";
import Vue from 'vue';
import slugify from 'slugify';
const Slug = {};
Slug.install = function(Vue) {
  Vue.prototype.$slugify = slugify;
 };
Vue.use(Slug);