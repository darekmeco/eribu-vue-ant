import Vue from 'vue';
import { message, notification } from "ant-design-vue";
const Fs = {};
Vue.prototype.$message = message;
Vue.prototype.$notification = notification;

Fs.install = function(Vue) {
  Vue.prototype.$message = message;
  Vue.prototype.$notification = notification;
};
Vue.use(Fs);
