import { Vue, Component, Mixins } from 'vue-property-decorator';
import _keys from 'lodash/keys';
import _pick from 'lodash/pick';
import ActionButtons from '@/components/form/ActionButtons.vue';

@Component({
  components: {
    ActionButtons,
  },
})
export default class FormMixin extends Vue {
  public $axios: any;
  public form: any;
  public $slugify: any;

  public data() {
    return {
      form: this.$form.createForm(this),
    };
  }
  public generateSlug(fieldName: string = 'name') {
    const name = this.form.getFieldValue(fieldName);
    if (name) {
      this.form.setFieldsValue({
        slug: this.$slugify(name),
      });
    }
  }
  public setDataFields(form: object, data: object) {
    this.form.setFieldsValue(_pick(data, _keys(form)));
  }
}
