import Vue from 'vue';
import Component from 'vue-class-component';
import { Modal } from 'ant-design-vue';

@Component
export default class Dialogs extends Vue {

  public confirmCustomDelete(cb: any) {
    Modal.confirm({
      title: 'Deleting item',
      content: 'Are you sure to delete item?',
      okText: 'Delete',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        cb();
      },
    });
  }
  public confirmCustomMove(cb: any) {
    Modal.confirm({
      title: 'Moving item',
      content: 'Are you sure to move item?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        cb();
      },
    });
  }
}
