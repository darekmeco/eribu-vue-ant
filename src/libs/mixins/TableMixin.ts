import Component from 'vue-class-component';
import Breadcrumb from '@/components/Breadcrumb.vue';
import Actions from '@/components/table/Actions.vue';
import Dialogs from '@/libs/mixins/Dialogs';
import FormatDate from '@/libs/mixins/FormatDate';
import { Mixins } from 'vue-property-decorator';
// You can declare a mixin as the same style as components.
@Component({
  components: {
    Breadcrumb,
    Actions,
  },
})
export default class TableMixin extends Mixins(Dialogs, FormatDate) {
    public $axios: any;
    public searchInput: string = '';
    get pagination() {
        return this.$store.state.table.pagination;
    }
    get items() {
        return this.$store.state.table.items;
    }
    get loading() {
        return this.$store.state.table.loading;
    }

    public goToEdit(id: string) {
        const base = location.pathname.replace('/index', '');
        this.$router.push(`${base}/edit/${id}`);
    }
    public deleteItem(id: string) {
        const base = location.pathname.replace('/index', '');
        this.$axios.delete(`${process.env.VUE_APP_APIURL}${base}/delete`, {
            params: {
                id,
            },
        }).then((res: any) => {
            this.$message.success('Item was successfully deleted');
            this.$store.dispatch('table/getItems');
        });
    }
    public onSearch(value: string) {
        this.searchInput = value;
        this.$store.commit('table/setPagination', {searchInput: value, current: 1});
        this.filterTable();
    }
    public handleTableChange(pagination: any, filters: any, sorter: any) {
        const pager = { ...this.pagination, searchInput: this.searchInput};
        pager.current = pagination.current;
        pager.field = sorter.columnKey ? sorter.columnKey : '';
        pager.orderBy = sorter.order ? sorter.order : '';

        this.$store.commit('table/setPagination', pager);
        this.filterTable();
    }
    public filterTable() {
        this.$store.dispatch('table/getItems');
    }

    public mounted() {
        this.$store.dispatch('table/getItems');
    }
}
