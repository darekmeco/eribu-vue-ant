import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
    filters: {
        formatPrice(value: any) {
            if (value === undefined) {
                value = 0;
            } else {
                value = parseInt(value, 10).toFixed(2).replace(/./g, (c, i, a) => {
                    return i && c !== "." && ((a.length - i) % 3 === 0) ? ' ' + c : c;
                });
            }
            return value + ' zł';
        },
    },
})
export default class FormatPrice extends Vue {}
