import { Vue, Component } from 'vue-property-decorator';

@Component
export default class SlugUnique extends Vue {
  public slugUnique: boolean = true;
  public form: any;

  public validateSlug(rule: any, value: any, callback: any) {
    if (value) {
      console.log('this.slugUnique: ', this.slugUnique);
      if (this.slugUnique) {
        return callback();
        // return;
      } else {
        callback('slug must be unique!');
      }
    }
    callback('slug is required');
    this.slugUnique = true;
  }
  public slugError() {
    this.slugUnique = false;
    this.form.validateFieldsAndScroll(['slug'], { force: true });
  }
}
