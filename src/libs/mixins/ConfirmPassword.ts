import { Vue, Component } from 'vue-property-decorator';

@Component
export default class ConfirmPassword extends Vue {

    public form: any;
    public confirmDirty: boolean = false;

    public handleConfirmBlur(e: any) {
        const value = e.target.value;
        this.confirmDirty = this.confirmDirty || !!value;
    }
    public compareToFirstPassword(rule: any, value: any, callback: any) {
        const form = this.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    }
    public validateToNextPassword(rule: any, value: any, callback: any) {
        const form = this.form;
        if (value && this.confirmDirty) {
            form.validateFields(['password_confirm'], {
                force: true,
            });
        }
        callback();
    }
}
