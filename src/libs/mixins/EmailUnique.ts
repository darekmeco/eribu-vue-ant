import { Vue, Component } from 'vue-property-decorator';

@Component
export default class EmailUnique extends Vue {

    public emailUnique: boolean = true;
    public form: any;

    public validateEmail(rule: any, value: any, callback: any) {
        if (value) {
            if (this.isValidEmail(value)) {
                if (this.emailUnique) {
                    callback();
                    return;
                }
                callback('email must be unique');
            }
            callback('email must be a valid address');
        }
        callback('email is required');
        this.emailUnique = true;
    }

    public emailError() {
        this.emailUnique = false;
        this.form.validateFieldsAndScroll(['email'], { force: true });
    }
    public isValidEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
}
