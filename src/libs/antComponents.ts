import Vue from 'vue';
import {
    Layout as antLayout,
    Breadcrumb,
    Menu,
    Icon,
    Avatar,
    Dropdown,
    Button,
    Row,
    Col,
    Form,
    Input,
    Switch,
    Select,
    Table,
    Divider,
  } from "ant-design-vue";
Vue.use(antLayout);
Vue.use(Breadcrumb);
Vue.use(Menu);
Vue.use(Icon);
Vue.use(Avatar);
Vue.use(Dropdown);
Vue.use(Button);
Vue.use(Row);
Vue.use(Col);
Vue.use(Form);
Vue.use(Input);
Vue.use(Switch);
Vue.use(Select);
Vue.use(Table);
Vue.use(Divider);
