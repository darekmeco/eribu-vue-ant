import File from '@/modules/media/models/File';

export interface FormsInterface {
    setDataFields(form: object, model: object): any;
}
