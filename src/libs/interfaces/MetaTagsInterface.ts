export interface MetaTagsIntergace {
    meta_title: string;
    meta_description: string;
    og_title: string;
    og_description: string;
    og_type: string;
}
