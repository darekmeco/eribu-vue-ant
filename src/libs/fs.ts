import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';

export default {
  getPathThumb(row: any) {
    const searchpath = _get(row, 'searchpath', null);
    const path = searchpath ? `/${searchpath}/` : '/';
    return `${process.env.VUE_APP_APIURL}/images${path}`;
  },
  getPathBig(row: any) {
    const searchpath = _get(row, 'searchpath', null);
    const path = searchpath ? `/${searchpath}/` : '/';
    return `${process.env.VUE_APP_APIURL}/assets${path}`;
  },
  getFilePathThumb(row = {}) {
    const filename = _get(row, 'filename', null);
    let searchpath = _get(row, 'searchpath', null);
    searchpath = _isEmpty(searchpath) ? '/' : '/' + searchpath;
    const path = searchpath + '/' + filename;
    return `${process.env.VUE_APP_APIURL}/images${path}`;
  },
  getFilePathBig(row: any) {
    const filename = _get(row, 'filename', null);
    let searchpath = _get(row, 'searchpath', null);
    searchpath = _isEmpty(searchpath) ? '/' : '/' + searchpath;
    const path = searchpath + '/' + filename;
    return `${process.env.VUE_APP_APIURL}/assets${path}`;
  },
  getPathFromObject(o: any, path: string, defpath = '/') {
    return _get(o, path, defpath);
  },
};
