import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import mediaStore from '@/modules/media/store';
import productStore from '@/modules/product/store';
import userStore from '@/modules/user/store';
import pageStore from '@/modules/page/store';
import blogStore from '@/modules/blog/store';
import menuStore from '@/modules/menu/store';
import tableStore from '@/libs/tableStore';
import _pick from 'lodash/pick';
import fs from "@/libs/fs.ts";
import _head from "lodash/fp/head";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    selectedKeys: [],
    openKeys: [],
    currentUser: '',
    avatar: '',
    breadcrumbs: [],
  },
  mutations: {
    setSidebarSelectedKeys(state, keys) {
      state.selectedKeys = keys;
    },
    setSidebarOpenKeys(state, keys) {
      if (keys[0]) {
          state.openKeys = keys;
      } else {
          state.openKeys = [];
      }
  },
    setCurrentUser(state, user) {
      state.currentUser = user;
    },
    setBreadcrumbs(state, breadcrumbs) {
      state.breadcrumbs = breadcrumbs;
    },
  },
  actions: {
    getCurrentUser(context: any) {
      const token = localStorage.getItem('jwt');
      if (token) {
        const base64 = token.split('.')[1];
        const user = JSON.parse(window.atob(base64));
        axios.get(`${process.env.VUE_APP_APIURL}/user/users/edit`, {
          params: {
            id: user._id,
          },
        }).then((res: any) => {
          context.commit('setCurrentUser', _pick(res.data.model, ['_id', 'firstname', 'lastname', 'email', 'singleFiles']));
        });
      }
    },
  },
  getters: {
    fullName(state) {
      let fullname = state.currentUser.firstname + ' ' + state.currentUser.lastname;
      while (fullname.includes('undefined')) {
        fullname = fullname.replace('undefined', '');
      }
      return fullname !== ' ' ? fullname : 'Fill your profile...';
    },
    avatar(state) {
      if (state.currentUser.singleFiles) {
        if (state.currentUser.singleFiles.length > 0) {
          return fs.getFilePathThumb(_head(state.currentUser.singleFiles));
        }
      }
    },
  },
  modules: {
    table: tableStore,
    media: mediaStore,
    product: productStore,
    user: userStore,
    page: pageStore,
    blog: blogStore,
    menu: menuStore,
  },
});
